#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int* foo(int q);
int* foo2(int q);
void foo3();
void foo4(int a);
void foo5(int* a);

int main(){
	
	//static variables
	foo3();
	foo3();
	foo3();
	printf("\n");
	

	//pointers and vars
	int a = 4;
	int* b = &a;

	printf(" b = %p\n",  b);
	printf("*b = %d\n", *b);
	printf(" a = %d\n\n",  a);

	//pass byoo5(b) value vs reference
	printf("Before foo4,  a = %d\n", a);
	foo4(a);
	printf("After foo4,   a = %d\n", a);

	printf("Before foo5, *b = %d\n", *b);
	foo5(b);
	printf("After foo5,  *b = %d\n", *b);
	printf("After foo5,   a = %d\n\n", a);


	//stack
	int* c = foo(3);
	printf(" c = %p\n", c);
	printf("*c = %d\n", *c);

	int* d = foo(8);
	printf(" d = %p\n", d);
	printf("*d = %d\n", *d);
	printf("*c = %d\n\n", *c);

	//malloc
	c = foo2(5);
	printf(" c = %p\n", c);
	printf("*c = %d\n", *c);

	d = foo2(8);
	printf(" d = %p\n", d);
	printf("*c = %d\n", *c);
	printf("\n");

	//calloc
	int* aa = (int *) malloc(sizeof(int)*4);
	int* bb = (int *) calloc(4, sizeof(int));
	for(int i = 0; i < 4; i++){
		printf("%d ", *(aa+i));
	}
	printf("\n");

	for(int i = 0; i < 4; i++){
		printf("%d ", *(bb+i));
	}
	printf("\n");

}


//impermanence of the stack
int* foo(int q){
  int  a = q*2;
  int* aa = &a;

  return aa;
}

//fix foo's problem using malloc
int* foo2(int q){
  int a = q*2;
  int* bb = (int *) malloc(sizeof(int));
  *bb = a;
  return bb;

}

//example of static variables
void foo3(){
	static int test = 0;
	printf("&test = %p\n", &test);
	printf("test  = %d\n", test);
	test++;
}

//pass by value vs reference
// - nothing important happens because we're incrementing the value
void foo4(int a){
	a++;
}

//pass by value vs reference
// - increment is permanent because we are working on the references to the
// variable,rather than the value of the variable
void foo5(int* a){
	(*a)++;
}
