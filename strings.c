#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(){

  /*
    There is no such thing as a String type in C, like there is in java. "Strings"
    are just arrays of characters in C.
    There are two different ways to declare them as seen below:

    An important note is strings must be null terminated, meaning the '\0' character
    must be at the end of the string.

    When declaring a "String" by character pointer as shown, there is an implicit
    '\0' placed at the end of the "String"

    However, when declaring it explicitly by an array, you have to be mindful of your
    inclusion of '\0' at the end, as it will not be added for you.
  */
  char* test = "Erik Andersson";
  char test2[] = {'E', 'r', 'i', 'k', ' '
  , 'A', 'n', 'd', 'e', 'r', 's', 's', 'o', 'n', '\0'};

  /*
    This block of prints shows how strings are traversed
    Since they're both pointers, the print starts at the beginning of test and
    test2, and prints characters until they see a null character
  */
  printf("char* test  = %s\n", test);
  printf("char test[] = %s\n", test2);
  printf("\n");

  /*
    This block of prints shows how char* declaration "gives" you the null character
    We print out the last character in the string, which should '\0' ("") for both lines
  */
  printf("test2[14]  = \"%c\"\n", test2[14]);
  printf("*(test+14) = \"%c\"\n", *(test+14));
  printf("\n");

  /*
    This last portion of prints is to show what can go wrong if you forget to
    null terminate your string, or access out of bounds of your array.
    Although test2 is an array of 15 characters, I can still access locations past
    14, unlike java.

    Because we do this, when we print we will keep spitting out characters until we
    see the next null character, which could be anywhere.
  */
  int i = 14;
	test2[i++] = '4';
	test2[i++] = '4';
	test2[i++] = '4';
	printf("%s\n", test2);

  /*
    here we're going to keep writing over stuff in our stack frame until
    the program kicks us out with a segmentation fault, because we went out of bounds
  */
	while(1){
  	test2[i++] = '4';
  	printf("%s\n", test2);
	}
	
}
