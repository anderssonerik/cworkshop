#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(){

  /*
    There are two ways to declare an array in c, as shown below.
  */
  int a[6] = {1, 8, 44, 32, 7, 14};
  int* b = a;


	printf("   a[] = %d", a[0]);
	for(int i = 1; i < 7; i++){
		printf(", %d", a[i]);
	}
	printf("\n");

	/*
    This block of prints is to show how you can access arrays using box (a[_])
    notation or pointer arithmetic to access portions of an array
  */

  printf("  a[0] = %d\n", a[0]);
  printf("    *b = %d\n", *b);
  printf("  a[1] = %d\n", a[1]);
  printf("*(b+1) = %d\n", *(b+1));
	printf("  *b+1 = %d\n", *b+1);

}
