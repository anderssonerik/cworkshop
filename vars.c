#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int* foo(int q){
  int  a = q*2;
  int* aa = &a;

  return aa;
}
int* foo2(int q){
  int a = q*2;
  int* bb = (int *) malloc(sizeof(int));
  *bb = a;
  return bb;

}

int main(){
  //pass by value
  int a = 4;
  int *b = &a;

  //pointers and vars
  //here we are doing ......
  printf("%p\n",  b);
  printf("%d\n", *b);
  printf("%d\n\n",  a);

  //stack
  int *c = foo(3);
  printf("%d\n", *c);
  int *d = foo(8);
  printf("%d\n", *c);
  printf("%d\n\n", *d);

  //malloc
  c = foo2(5);
  printf("%d\n", *c);
  foo2(8);
  printf("%d\n", *c);
}
