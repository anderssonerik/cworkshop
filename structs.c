#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

/*
  Below is the outline for declaring the form for a struct.
  We do this in global scope so we can delcare a "Foo" struct anywhere in our program
*/
struct Foo{
  char* name;
  int size;
  double bigSize;
  char box[4];
};

// test
// Preconditions:
// - a is a pointer to a struct
// Postconditions:
// - a's name is changed to "Bobby"
// - a's size is changed to 55
// - a's bigSize is reduced by 20000
// - a's box array is changed to {'b', 'a', 't', '\0'}
void test(struct Foo *a){
  a->name = "Bobby";
  (*a).size = 55;
  a->bigSize -= 20000.00;
  a->box[0] = 'b';
  a->box[1] = 'a';
  a->box[2] = 't';

}

int main(){

  //here we initialize a struct, like saying "int a;"
  struct Foo foo;

  //here we assign values to each variable in the struct
  foo.name = "Erik";
  foo.size = 42;
  foo.bigSize = 40000.00;
  foo.box[0] = 'a';
  foo.box[1] = 't';
  foo.box[2] = 'e';
  foo.box[3] = '\0';

  /*
    This section of prints shows the values of the variables in the struct before
    and after alteration by test()
  */
  printf("%s %d %f %s\n", foo.name, foo.size, foo.bigSize, foo.box);
  test(&foo);
  printf("%s %d %f %s\n", foo.name, foo.size, foo.bigSize, foo.box);
}
